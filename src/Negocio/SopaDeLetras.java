/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;
import com.sun.org.apache.xerces.internal.impl.xs.opti.DefaultText;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultStyledDocument;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.*;

/**
 *
 * @author jefferson
 */
public class SopaDeLetras {

    Workbook book;
    // instance variables - replace the example below with your own
    private char sopas[][];

    /**
     * Constructor for objects of class SopaDeLetras
     */
    public SopaDeLetras() {

    }

    public SopaDeLetras(String palabras) throws Exception {
        if (palabras == null || palabras.isEmpty()) {
            throw new Exception("Error no se puede crear la matriz de char para la sopa de letras");
        }

        //Crear la matriz con las correspondientes filas:
        String palabras2[] = palabras.split(",");
        this.sopas = new char[palabras2.length][];
        int i = 0;
        for (String palabraX : palabras2) {
            //Creando las columnas de la fila i
            this.sopas[i] = new char[palabraX.length()];
            pasar(palabraX, this.sopas[i]);
            i++;

        }

    }

    private void pasar(String palabra, char fila[]) {

        for (int j = 0; j < palabra.length(); j++) {
            fila[j] = palabra.charAt(j);
        }
    }

    public String toString() {
        String msg = "";
        for (int i = 0; i < this.sopas.length; i++) {
            for (int j = 0; j < this.sopas[i].length; j++) {
                msg += this.sopas[i][j] + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public String toString2() {
        String msg = "";
        for (char filas[] : this.sopas) {
            for (char dato : filas) {
                msg += dato + "\t";
            }

            msg += "\n";

        }
        return msg;
    }

    public boolean esCuadrada() {
        int contF = this.sopas.length;
        if (this.sopas.length == 1) {
            return false;
        }
        for (int j = 0; j < this.sopas.length; j++) {
            if (this.sopas[j].length != contF) {
                return false;
            }
        }
        return true;
    }

    public boolean esDispersa() {
        boolean dispersa = false;
        int aux = this.sopas[0].length;
        if (this.sopas.length == 1) {
            return dispersa;
        } else {
            for (int i = 0; i < this.sopas.length; i++) {

                if (this.sopas[i].length != aux) {
                    dispersa = true;
                } else {
                    dispersa = false;
                }

            }
        }
        //System.out.println("dispersa");
        return dispersa;
    }

    public boolean esRectangular() {
        int contF = this.sopas.length;
        int contC = this.sopas[0].length;
        if (this.sopas.length == 1) {
            return false;
        }
        for (int i = 0; i < this.sopas.length; i++) {
            if (this.sopas[i].length != contC) {
                return false;
            }
        }
        if (contF != contC) {
            return true;
        }
        return false;
    }

    /*
        retorna cuantas veces esta la palabra en la matriz
     */
    public int getContar(String palabra) {
        return 0;
    }

    /*
        debe ser cuadrada sopas
     */
    public char[] getDiagonalPrincipal() throws Exception {
        //si y solo si es cuadrada , si no, lanza excepcion
        if (!esCuadrada())//validamos si es cuadrada
        {
            //return null;
            throw new Exception("No se puede imprimir la diagonal");

        }
        int a = 0;
        char msg[] = new char[this.sopas[0].length];//

        for (int i = 0; i < this.sopas.length; i++) {
            //msg=new char[this.sopa.length];
            for (int j = 0; j < this.sopas[i].length; j++) {
                if (i == j) {
                    msg[a] = this.sopas[i][j];
                    a++;

                }

            }
        }
        //System.out.println("diagonal");
        return msg;
    }

    public int getCantidadCol(int fila) {
        if (this.sopas == null || fila < 0) {
            return -1;
        }
        return sopas[fila].length;
    }

    public int filaMasDatos() {
        int num = 0;
        int fila = 0;
        if (this.sopas == null) {
            return -1;
        }//balidamos para el test
        for (int i = 0; i < this.sopas.length; i++) {
            if (num == 0) {
                num = getCantidadCol(i);
            } else {
                if (num < getCantidadCol(i)) {
                    num = getCantidadCol(i);
                    fila = i;
                }

            }
        }
        if (fila != 0) {
            return fila + 1;
        }//se retorna la fila con mas variables
        return num;
    }

    //Start GetterSetterExtension Source Code
    /**
     * GET Method Propertie sopas
     */
    public char[][] getSopas() {
        return this.sopas;
    }//end method getSopas

    //End GetterSetterExtension Source Code
//!
    public void buscarPalabra(String palabra) throws Exception
    {
        if(esDispersa() || palabra.isEmpty()){
            throw new Exception("No se puede buscar la palabra");
        }
        System.out.println("---horizontales---");
        buscarPalabraHorizontal(palabra.toCharArray(),sopas);
        buscarPalabraHorizontalContrario(palabra.toCharArray(),sopas);
        System.out.println("---verticales---");
        buscarPalabraVertical(palabra.toCharArray(),sopas);
        buscarPalabraVerticalContrario(palabra.toCharArray(),sopas);
        System.out.println("---diagonales---");
        buscarPalabraDiagonal1(palabra.toCharArray(), sopas);
        buscarPalabraDiagonal2(palabra.toCharArray(), sopas);
        buscarPalabraDiagonal3(palabra.toCharArray(), sopas);
        buscarPalabraDiagonal4(palabra.toCharArray(), sopas);
    }

    public void buscarPalabraHorizontal(char[] palabra, char[][] vector) {
        boolean aux=false;
        int aux1;
        int aux2;
        int con;
        for (int i = 0; i < vector.length; i++){	
            for (int j = 0; j < vector[0].length; j++){	
//		aux1=i;
                aux2=j;
                con=0;
                while( con!= palabra.length){
                    if (aux2 < vector[0].length && vector[i][aux2] == palabra[con]) {
//                        aux1++;
                        aux2++;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con = palabra.length;
                    }
                }
                if(aux!=false){
                    System.out.println("se encuentra horizontal izq a der "
                            +"iniciando  en la fila: "+(i+1)+" y columna: "+(j+1));
//                    i=vector.length;
                }
            }
        }
    }

    public void buscarPalabraHorizontalContrario(char[] palabra, char[][] vector) {
        boolean aux=false;
        int aux2;
        int con;
        for (int i = 0; i < vector.length; i++){	
            for (int j = vector[0].length-1; j >=0 ; j--){	
		aux2=j;
                con=0;
                while( con!= palabra.length){
                    if (aux2>=0 && vector[i][aux2] == palabra[con]) {
                        aux2--;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con = palabra.length;
                    }
                }
                if(aux!=false ){
                    System.out.println("se encuentra horizontal der a izq "
                            +"iniciando  en la fila: "+(i+1)+" y columna: "+(j+1));
//                    i=vector.length;
                }
            }
        }
    }

    public void buscarPalabraVertical(char []palabra, char [][]vector){
        boolean aux=false;
        int i,j,con;
        int aux2;
        for (i = 0; i < vector[0].length; i++){	
            for (j = 0; j < vector.length; j++){
                aux2=j;
                con=0;
                while(con!= palabra.length){
                    if (aux2<vector.length && vector[aux2][i] == palabra[con]) {
                        aux2++;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con = palabra.length;
                    }
                }
                if(aux!=false){
                System.out.println("se encuentra de arriba a abajo iniciando "
                        +"en la fila: "+(j+1)+" y columna: "+(i+1));
                }
                
            }
        }
        
    }
    
    public void buscarPalabraVerticalContrario(char []palabra, char [][]vector){
        boolean aux=false;
        int i,j,con;
        int aux2;
        for (i= vector[0].length-1;i>=0; i--){	
            for (j = vector.length-1;j>=0; j--){
                aux2=j;
                con=0;
                while(con!= palabra.length){
                    if(aux2>=0 && vector[aux2][i]==palabra[con]){
                        aux2--;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con=palabra.length;
                    }
                }
                if(aux!=false){
                    System.out.println("se encuentra de abajo a arriba iniciando "
                        +"en la fila: "+(j+1)+" y columna: "+(i+1));
                }
            }
        }
        
    }
    
    public void buscarPalabraDiagonal1(char []palabra, char [][]vector){
        boolean aux=false;
        int aux1;
        int aux2;
        int con;
        for (int i = 0; i < vector.length-1; i++){	
            for (int j = 0; j < vector[0].length-1; j++){	
		aux1=i;
                aux2=j;
                con=0;
                while(con!= palabra.length){
                    if (aux1 < vector.length && aux2 < vector[0].length && vector[aux1][aux2] == palabra[con]) {
//                    if (vector[aux1][aux2] == palabra[con]) {
                        aux1++;
                        aux2++;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con = palabra.length;
                    }
                }
                if(aux!=false){
                    System.out.println("se encuentra diagonal izq a der de arriba abajo "
                            +"iniciando en la fila: "+(i+1)+" y columna: "+(j+1));
                }
            }
        }
    }
    
    public void buscarPalabraDiagonal2(char []palabra, char [][]vector){
        boolean aux=false;
        int aux1;
        int aux2;
        int con;
        for (int i = 0; i < vector.length; i++){	
            for (int j = vector[0].length-1; j >=0; j--){	
		aux1=i;
                aux2=j;
                con=0;
                while(con != palabra.length){
                    if (aux1 < vector.length && aux2 >=0 && vector[aux1][aux2] == palabra[con]) {
                        aux1++;
                        aux2--;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con = palabra.length;
                    }
                }
                if(aux!=false){
                    System.out.println("se encuentra diagonal der a izq de arriba abajo "
                            +"iniciando en la fila: "+(i+1)+" y columna: "+(j+1));
                }
            }
        }
    }
    
    public void buscarPalabraDiagonal3(char []palabra, char [][]vector){
        boolean aux=false;
        int aux1;
        int aux2;
        int con;
        for (int i = vector.length-1; i >=0; i--){	
            for (int j = vector[0].length-1; j >=0 ; j--){	
		aux1=i;
                aux2=j;
                con=0;
                while(con != palabra.length){
                    if (aux1 >=0 && aux2 >=0 && vector[aux1][aux2] == palabra[con]) {
                        aux1--;
                        aux2--;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con = palabra.length;
                    }
                }
                if(aux!=false){
                    System.out.println("se encuentra diagonal der a izq de abajo arriba "
                            +"iniciando en la fila: "+(i+1)+" y la columna: "+(j+1));
                }
            }
        }
    }
    
    public void buscarPalabraDiagonal4(char []palabra, char [][]vector){
        boolean aux=false;
        int aux1;
        int aux2;
        int con;
        for (int i = vector.length-1; i >=0; i--){	
            for (int j=0; j<=vector[0].length-1 ; j++){	
		aux1=i;
                aux2=j;
                con=0;
                while(con != palabra.length){
                    if ( aux1 >=0 && aux2<=vector[0].length-1 && vector[aux1][aux2] == palabra[con]) {
                        aux1--;
                        aux2++;
                        aux=true;
                        con++;
                    }else{
                        aux=false;
                        con = palabra.length;
                    }
                }
                if(aux!=false){
                    System.out.println("se encuentra diagonal izq a der de abajo arriba "
                            +"iniciando en la fila: "+(i+1)+" y la columna: "+(j+1));
                }
            }
        }
    }
    
public String Importar(File archivo, JTable tabla  ){
       
        String mensaje="no se puede importar";
        DefaultTableModel modelo=new DefaultTableModel();
        tabla.setModel(modelo);
       
       
        
       
        try {
            //CREA ARCHIVO CON EXTENSION XLS Y XLSX
            book=WorkbookFactory.create(new FileInputStream(archivo));
            Sheet hoja=book.getSheetAt(0);
            Iterator FilaIterator=hoja.rowIterator();
            int IndiceFila=-1;
            
            //VA SER VERDADERO SI EXISTEN FILAS POR RECORRER
            while (FilaIterator.hasNext()) {                
                //INDICE FILA AUMENTA 1 POR CADA RECORRIDO
                IndiceFila++;
                Row fila=(Row)FilaIterator.next();
                //RECORRE LAS COLUMNAS O CELDAS DE UNA FILA YA CREADA
                Iterator ColumnaIterator=fila.cellIterator();
                //ASIGNAMOS EL MAXIMO DE COLUMNA PERMITIDO
                Object[]ListaColumna=new Object[9999];
                int IndiceColumna=-1;
                //VA SER VERDADERO SI EXISTEN COLUMNAS POR RECORRER
                while (ColumnaIterator.hasNext()) {                    
                    //INDICE COLUMNA AUMENTA 1 POR CADA RECORRIDO
                    IndiceColumna++;
                    Cell celda=(Cell)ColumnaIterator.next();
                    //SI INDICE FILA ES IGUAL A "0" ENTONCES SE AGREGA UNA COLUMNA
                    if(IndiceFila==0){
                        modelo.addColumn(celda.getStringCellValue());
                    }else{
                        if(celda!=null){
                            switch (celda.getCellType()){
                                case Cell.CELL_TYPE_NUMERIC:
                                    ListaColumna[IndiceColumna]=(int)Math.round(celda.getNumericCellValue());
                                    break;
                                case Cell.CELL_TYPE_STRING:
                                    ListaColumna[IndiceColumna]=celda.getStringCellValue();
                                    break;
                                case Cell.CELL_TYPE_BOOLEAN:
                                    ListaColumna[IndiceColumna]=celda.getBooleanCellValue();
                                    break;
                                    default:
                                        ListaColumna[IndiceColumna]=celda.getDateCellValue();
                                        break;
                            }
                        }
                    }
                }
                
                if(IndiceFila!=0)modelo.addRow(ListaColumna);
            }
            mensaje="Importacion Exitosa";
            
        } catch (Exception e) {
        }
        return mensaje;
    }



}
