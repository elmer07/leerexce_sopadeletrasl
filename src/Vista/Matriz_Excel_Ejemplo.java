/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Negocio.SopaDeLetras;
import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.*;

/**
 *
 * @author jefferson
 */
public class Matriz_Excel_Ejemplo {
    
    /**
     * @param args the command line arguments
     */
    static String sopa="";
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        leerExcel();
        try{
            SopaDeLetras s=new SopaDeLetras(sopa);
            System.out.println(s.esCuadrada());
//            System.out.println(s.esRectangular());
//            System.out.println(s.esDispersa());
            System.out.println("---------------");
            
//            s.buscarPalabraH("MAR");
//            s.buscarPalabraV("MAR");
            s.buscarPalabra("MAR");
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    
     private static void leerExcel() throws Exception {
//     private static void leerExcel(File archivo) throws Exception {
        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream("data.xls"));
//        HSSFWorkbook archivoExcel = new HSSFWorkbook(new FileInputStream(archivo));
        //Obtiene la hoja 1
        HSSFSheet hoja = archivoExcel.getSheetAt(0);
        //Obtiene el número de la última fila con datos de la hoja.
        int canFilas = hoja.getLastRowNum()+1;
        System.out.println("Filas:"+canFilas);
        for (int i = 0; i < canFilas; i++) {
            //Obtiene el índice de la última celda contenida en esta fila MÁS UNO.
            HSSFRow filas = hoja.getRow(i);
            //Obtiene la cantidad de colomunas de esa fila
            int cantCol=filas.getLastCellNum();
            
        for(int j=0;j<cantCol;j++)    
        {
            //Obtiene la celda y su valor respectivo
            //double r=filas.getCell(i).getNumericCellValue();
            String valor=filas.getCell(j).getStringCellValue();
            Matriz_Excel_Ejemplo.sopa+=valor;
            System.out.print(valor+"\t");
        }
        sopa+=",";
     System.out.println();
       }
    }
    
    
}
